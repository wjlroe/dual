#include <pebble.h>

#define EDGE_PADDING     2

static Window *s_main_window;
static TextLayer *s_twelve_layer;
static TextLayer *s_three_layer;
static TextLayer *s_six_layer;
static TextLayer *s_nine_layer;

typedef struct {
  int month;
  int day;
  int hour;
  int minute;
  int second;
} DateTime;

static DateTime s_current_time;

static void main_window_load(Window *window)
{
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);
  int mid_x = bounds.size.w / 2;
  int mid_y = bounds.size.h / 2;
  //GFont hour_font = fonts_get_system_font(FONT_KEY_LECO_32_BOLD_NUMBERS);
  GFont hour_font = fonts_get_system_font(FONT_KEY_LECO_20_BOLD_NUMBERS);
  //GFont hour_font = fonts_get_system_font(FONT_KEY_LECO_26_BOLD_NUMBERS_AM_PM);

  TextLayer *s_test_layer = text_layer_create(GRect(0, 0, 100, 100));
  text_layer_set_text(s_test_layer, "12");
  text_layer_set_font(s_test_layer, hour_font);
  text_layer_set_text_alignment(s_test_layer, GTextAlignmentCenter);
  //layer_add_child(window_layer, text_layer_get_layer(s_test_layer));

  GSize hour_size = text_layer_get_content_size(s_test_layer);
  int hour_width = hour_size.w;
  int hour_height = hour_size.h;

  APP_LOG(APP_LOG_LEVEL_INFO,
          "Hour_width = %d, hour_height = %d",
          hour_width,
          hour_height);

  text_layer_destroy(s_test_layer);

  s_twelve_layer = text_layer_create(GRect(mid_x - (hour_width / 2),
                                           EDGE_PADDING,
                                           hour_width,
                                           hour_height));
  text_layer_set_background_color(s_twelve_layer, GColorBlack);
  text_layer_set_text_color(s_twelve_layer, GColorWhite);
  text_layer_set_text(s_twelve_layer, "12");
  text_layer_set_font(s_twelve_layer, hour_font);
  text_layer_set_text_alignment(s_twelve_layer, GTextAlignmentCenter);

  layer_add_child(window_layer, text_layer_get_layer(s_twelve_layer));

  s_three_layer = text_layer_create(GRect(bounds.size.w - hour_width,
                                          mid_y - (hour_height / 2),
                                          hour_width,
                                          hour_height));
  text_layer_set_background_color(s_three_layer, GColorBlack);
  text_layer_set_text_color(s_three_layer, GColorWhite);
  text_layer_set_text(s_three_layer, "3");
  text_layer_set_font(s_three_layer, hour_font);
  text_layer_set_text_alignment(s_three_layer, GTextAlignmentCenter);

  layer_add_child(window_layer, text_layer_get_layer(s_three_layer));
}

static void main_window_unload(Window *window)
{
  text_layer_destroy(s_twelve_layer);
  text_layer_destroy(s_three_layer);
}

static void init()
{
  s_main_window = window_create();

  window_set_window_handlers(s_main_window, (WindowHandlers) {
        .load = main_window_load,
          .unload = main_window_unload
          });

  window_set_background_color(s_main_window, GColorBlack);

  window_stack_push(s_main_window, true);
}

static void deinit()
{
  window_destroy(s_main_window);
}

int main(void) {
  init();
  app_event_loop();
  deinit();
}
